# Abstract
The purpose of this report is to introduce the functions of the players’ side of ANU Basketball. It will illustrate how the frontend and the backend interact within the players’ side. The structure behind our product can be seen as a module-view-controller model. Therefore, we would like to introduce and explain our code in relevant format. Please be notified that all controller functions are stored in `PlayerCompetitionsController`.
# List of Current Page
- Index
  - Route: `/` or `/home`
  - View: `welcome`
  - Controller: `PlayerCompetitionsController@index`
- Details
  - Route: `/competitions/{id}`
  - View: `player.competitions.details`
  - Controller: `PlayerCompetitionsController@show`
- Draws
  - Route: `/competitions/{id}/draws`
  - View: `player.competitions.draws`
  - Controller: `PlayerCompetitionsController@showDraws`
- Join
  - Route: `/competitions/{id}/join`
  - View: `player.competitions.join`
  - Controller: `PlayerCompetitionsController@create`
- team
  - Route: `/competitions/{id}/teams`
  - View: `player.competitions.teams`
  - Controller: `PlayerCompetitionsController@showTeams`
- Contact
  - Route: `/competitions/{id}`
  - View: `player.competitions.contacts`
  - Controller: `PlayerCompetitionsController@showContact`

# Specification of each controller
1. `PlayerCompetitionsController@index`
 - parameters: `null`
 - return:
     - view: `welcome`
     - compact: `Array[Object]::competitions`, `Carbon::now`
2. `PlayerCompetitionsController@show`
 - parameters: `Integer::competition_id`
 - return:
     - view: `player.competitions.details`
     - compact: `Object::competition`, `Carbon::now`
3. `PlayerCompetitionsController@showContact`
 - parameters: `Integer::competition_id`
 - return:
     - view: `player.competitions.contacts`
     - compact: `Object::competition`, `Array[Object]::contacts`, `Carbon::now`
4. `PlayerCompetitionsController@create`
 - parameters: `Integer::competition_id`
 - return:
     - view: `player.competitions.join`
     - compact: `Object::competition`, `Carbon::now`
5. `PlayerCompetitionsController@showTeams`
 - parameters: `Integer::competition_id`
 - return:
     - view: `player.competitions.teams`
     - compact: `Object::competition`, `Array[Object]::players`, `Carbon::now`, `Array[Object]::teams`, `Integer::count_player`, `Integer::count_team`
6. `PlayerCompetitionsController@showDraws`
 - parameters: `Integer::competition_id`
 - return:
     - view: `player.competitions.draws`
     - compact: `Object::competition`, `Carbon::now`, `Array[Object]::Ladders`, `Array[Object]::Draws`
 - The method to calculate the ladders:
     - GP = Number of games played by a team
     - W = Number of games won by a team
     - D = Number of games drawn by a team
     - L = Number of games Lost by a team
     - GF = Overall number of points a team has got
     - GA = Overall number of points a team has got agains it
     - GD = GF - GA
     - PTS = W * 2 + D * 1 + L * 0
 - Ladders is sorted by PTS 