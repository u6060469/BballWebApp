# Noah's reflection of the year
I am super impressed that the project came together as a group effort. Our output was a collaborative effort in which each team member contributed. We have been able to achieve a product that is consistent in design and will be a useful tool in running social sport competitions. 


I have loved working with five fantastic guys over this year and building great friendships. They have displayed fantastic characteristics. They have been patient, professional, hard-working, full of great ideas. There have been many challenges but I think we are proud with what we have achieved. 


Most of our team members did not have much experience with web development before this year, so we have had to spend a lot of time learning the technology (e.g. such as Laravel framework). This meant managing milestones was difficult, as there was uncertainty in the time a task would take due to not having a point of reference (e.g. prior experience). 

As the client, I came into the project without a clear vision of what the application would look like, this has ment the requirements were quite flexible and changed through out the year. I think we had the healthy approach of not becoming too attached to our output. This allowed us to develop an application that fitted my expectations. 


Laravel has been a great choice. It is the perfect level of abstraction. We were able to have complete control of the design but implement complex features. Having this project was a great motivator for me to complete an online course on laravel and learn something useful about web development.


There have been many a time were we have felt lost and directionless  but I think we consistently worked to get advice and come up with solutions to keep the ball rolling. Being paired up with an amazing mentor like David has been so helpful. David has been able to see our situation and give advice accordingly. David through his advice gave us the opportunity to really think about our design before worrying about how thinks would be implemented. He gave us the flexibility to work it out for ourselves, but kept us progressing. We also had advice coming from our tutors. Our semester 1 tutor Todd really pushed us to develop our communication and project management. He also pushed us to see this product through the view of it being a small start-up.  Our semester 2 tutor Elena really pushed us to get a deliverable product. She was able to give us great advice on web development due to her many years in the industry. It has been great to be able to be exposed to professionals from industry, talking to our tutors and mentors about what they do for work, their ideas and options, etc has been really interesting. 


