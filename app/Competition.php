<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Competition extends Model
{
    protected $fillable = [
        'name', 'rego_open', 'rego_close'
    ];

    protected $dates = ['created_at', 'updated_at', 'rego_open', 'rego_close'];


    public function contacts(){
        return $this->hasMany('App\Contact');
    }

    public function details(){
        return $this->hasMany('App\Detail');
    }

    public function teams(){
        return $this->hasMany('App\Team');
    }

        public function players(){
        return $this->hasMany('App\Player');
    }

    public function matches(){
        return $this->hasMany('App\Match');
    }

}


