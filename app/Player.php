<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    protected $fillable = [
        'competition_id',
        'team_id',
        'paid',
        'name',
        'email',
        'ability',
        'teammate'
    ];
    public function team(){
        return $this->belongsTo('App\Team');
    }
}
