<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'name', 'role', 'mobile', 'email', 'competition_id',
    ];
}
