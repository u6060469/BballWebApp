<?php

namespace App\Http\Controllers;

use App\Competition;
use App\Http\Requests\TeamsRequest;
use App\Player;
use App\Team;
use Illuminate\Http\Request;

class AdminTeamsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($competition_id)
    {
        $competition = Competition::findORFail($competition_id);
        return view('admin.competitions.teams.index', compact('competition'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($competition_id)
    {
        $competition = Competition::findORFail($competition_id);
        return view('admin.competitions.teams.create', compact('competition'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeamsRequest $request)
    {
        $input = $request->all();
        $team = Team::create($input);
        flash('Team '.$team->name.' has been created')->success();
        return redirect('/admin/competitions/' . $input['competition_id'] . '/teams');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($competition_id, $id)
    {
        $competition = Competition::findORFail($competition_id);
        $team = Team::findOrFail($id);
        return view('admin.competitions.teams.edit', compact('team', 'competition'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TeamsRequest $request, $id, $competition_id)
    {
        $input = $request->all();
        $team = Team::findOrFail($id);
        $team->update($input);
        flash('Information for '.$team->name.' has been updated')->success();
        return redirect('/admin/competitions/' . $competition_id . '/teams');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $competition_id)
    {

        $team = Team::findOrFail($id);
        flash('Information for '.$team->name.' has been deleted')->success();
        $team->delete();
        return redirect('/admin/competitions/' . $competition_id . '/teams');
    }

    /**
     * Retreive relevant data for easy sort
     *
     * @param Request
     * @return json
     */
    public function getDataForEasySort(Request $request)
    {
        $result = new \stdClass();
        $result->plays = Player::where('competition_id', $request['competition_id'])->get();
        $result->teams = Team::where('competition_id', $request['competition_id'])->get();
        return response()->json([
            'data' => $result,
        ], 200);
    }

    /**
     * Update the player-team allocation by easy sort
     * 
     * @param Request
     * @return json
     */
    public function storeDataFromEasySort(Request $request)
    {
        $arrTeamPlayers = $request['params']['arrTeamPlayers'];
        $competition_id = $request['params']['competition_id'];
        foreach ($arrTeamPlayers as $record) {
            if (!empty($record['players'])) {
                foreach ($record['players'] as $player) {
                    Player::where('id', $player['user_id'])->update(['team_id' => $record['team_id']]);
                }
            }
        }
        return response()->json([
            'status' => 'success',
        ], 200);
    }
}
