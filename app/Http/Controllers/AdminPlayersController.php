<?php

namespace App\Http\Controllers;

use App\Competition;
use App\Http\Requests\PlayersRequest;
use App\Player;
use App\Team;
use Illuminate\Http\Request;

class AdminPlayersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($competition_id)
    {

       // $teams = Team::pluck('name', 'id')->all();
        $competition = Competition::findORFail($competition_id);
        $teams = $competition->teams->pluck('name', 'id')->all();
        $competition->players = $competition->players->sortBy('name')->sortBy('paid');

        return view('admin.competitions.players.index', compact('competition', 'teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($competition_id)
    {


        $competition = Competition::findORFail($competition_id);
        $teams = $competition->teams->pluck('name', 'id')->all();
        return view('admin.competitions.players.create', compact('competition', 'teams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PlayersRequest $request, $competition_id)
    {
        $input = $request->all();
        $player = Player::create($input);
        flash('Player '.$player->name.' has been created for this competition')->success();
        return redirect('/admin/competitions/'.$input['competition_id'].'/players');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($competition_id, $id)
    {

        $competition = Competition::findORFail($competition_id);
        $player = Player::findOrFail($id);
        $teams = $competition->teams->pluck('name', 'id')->all();
        return view('admin.competitions.players.edit', compact('player', 'competition', 'teams'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $competition_id)
    {
        $input = $request->all();
        $player = Player::findOrFail($id);
        $player->update($input);
       // Player::findOrFail($id)->update($request->all());
       flash('Information for '.$player->name.' has been updated')->success();
        return redirect('/admin/competitions/'.$competition_id.'/players');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $competition_id)
    {
        $player = Player::findOrFail($id);
        $player->delete();

        return redirect('/admin/competitions/'.$competition_id.'/players');

    }
}
