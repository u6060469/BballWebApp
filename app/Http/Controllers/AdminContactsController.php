<?php

namespace App\Http\Controllers;

use App\Competition;
use App\Contact;
use App\Http\Requests\ContactsRequest;
use Illuminate\Http\Request;

class AdminContactsController extends Controller
{
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($competition_id)
    {

        $competition = Competition::findORFail($competition_id);

        return view('admin.competitions.contacts.index', compact('competition'));
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($competition_id)
    {
        $competition = Competition::findORFail($competition_id);
       return view('admin.competitions.contacts.create', compact('competition'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContactsRequest $request, $competition_id)
    {
        $input = $request->all();
        Contact::create($input);
        return redirect('/admin/competitions/'.$competition_id.'/contacts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($competition_id, $id)
    {

        $competition = Competition::findORFail($competition_id);
        $contact = Contact::findOrFail($id);
        return view('admin.competitions.contacts.edit', compact('contact', 'competition'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ContactsRequest $request, $id, $competition_id)
    {



        $input = $request->all();
        $contact = Contact::findOrFail($id);
        $contact->update($input);

        return redirect('/admin/competitions/'.$competition_id.'/contacts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $competition_id)
    {
        $contact = Contact::findOrFail($id);
        $contact->delete();

        return redirect('/admin/competitions/'.$competition_id.'/contacts');

    }
}
