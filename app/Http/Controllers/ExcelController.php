<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Competition;
use Excel;

class ExcelController extends Controller
{
    public function export($id){

        $players = \DB::table('players')
        ->leftjoin('teams', 'teams.id', '=', 'players.team_id')
        ->select('teams.name as team_name','email','players.name as player_name','paid')
        ->where('players.competition_id',$id)
        ->get();

        $cellData = [
            ['player name','email','paid','team name'],
        ];
        foreach($players as $player){
            array_push($cellData,[$player->player_name,$player->email,$player->paid == 1?"yes":"no",$player->team_name]);
        } 

        $competition = Competition::find($id);
        Excel::create($competition->name,function($excel) use ($cellData){
            $excel->sheet('contact', function($sheet) use ($cellData){
                $sheet->rows($cellData);
            });
        })->export('xls');
    }
}
