<?php

namespace App\Http\Controllers;

use App\Competition;
use App\Match;
use App\Team;
use Illuminate\Http\Request;

class AdminMatchesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($competition_id)
    {

        $competition = Competition::findORFail($competition_id);

        $matches = \DB::table('teams as guest')
            ->rightjoin('matches', 'matches.guest_team_id', '=', 'guest.id')->where('matches.competition_id', $competition_id)
            ->leftjoin('teams as home', 'matches.home_team_id', '=', 'home.id')
            ->select('matches.*', 'guest.name as guest_name', 'home.name as home_name', 'matches.id as id')
            ->orderBy('round', 'asc')
            ->get();
        $teams = $competition->teams->pluck('name', 'id')->all();
        return view('admin.competitions.matches.index', compact('competition', 'matches', 'teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($competition_id)
    {
        $competition = Competition::findORFail($competition_id);
        $teams = $competition->teams->pluck('name', 'id')->all();
        return view('admin.competitions.matches.create', compact('competition', 'teams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'round' => 'required',
        ]);

        $input = $request->all();
        $match = Match::create($input);
        $homeTeam = Team::findOrFail($match->home_team_id)->name;
        $guestTeam = Team::findOrFail($match->guest_team_id)->name;
        flash('Match '.$homeTeam.' vs '.$guestTeam.' has been created')->success();
        return redirect('/admin/competitions/' . $input['competition_id'] . '/matches');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $competition_id)
    {
        $input = $request->all();
        $detail = Match::findOrFail($id);
        $detail->update($input);
        $homeTeam = Team::findOrFail($detail->home_team_id)->name;
        $guestTeam = Team::findOrFail($detail->guest_team_id)->name;
        flash('Match '.$homeTeam.' vs '.$guestTeam.' has been updated')->success();
        return redirect('/admin/competitions/' . $competition_id . '/matches');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $match_id)
    {
        $match = Match::findOrFail($match_id);
        $homeTeam = Team::findOrFail($match->home_team_id)->name;
        $guestTeam = Team::findOrFail($match->guest_team_id)->name;
        $match->delete();
        flash('Match '.$homeTeam.' vs '.$guestTeam.' has been updated')->success();
        return redirect('/admin/competitions/' . $id . '/matches');
    }

    public function clear($id)
    {
        Match::where('competition_id', $id)->delete();

        return redirect('/admin/competitions/' . $id . '/matches');
    }

    public function autoCreate($id)
    {


        $competition = Competition::findOrFail($id);

        $teams =  count($competition->teams);


        return view('admin.competitions.matches.create_automatic', compact('competition', 'teams'));
    }

    public function store_automatic(Request $request)
    {
        $this->validate($request, [
            'matches' => 'required',
        ]);

        $input = $request->all();
        $id = $input['competition_id'];

        $teams = Team::where('competition_id', $id)->select('id')->get();
        $ids = [];
        foreach ($teams as $team) {
            array_push($ids, $team->id);
        }
        //number of teams
        $n = sizeof($ids);

        //initial the schedule
        $schedule = [];
        $map = [];
        $m = intval($n / 2);
        for ($i = 0; $i <= $n; $i++) {
            array_push($map, []);
            array_push($schedule, []);
        }

        $schedule = $this->GSchedule($schedule, $n);
        $matches = $input['matches'];
        $loop = 0;
        $round = 1;
        while ($matches > 0) {
            for ($j = 0; $j <= $n; $j++) {
                for ($i = 0; $i < intval(($n + 1) / 2); $i++) {
                    if ($schedule[$j] != []) {
                        if ($schedule[$j][$i][0] <= $n && $schedule[$j][$i][1] <= $n && $matches > 0) {
                            $match = new Match;
                            $match->competition_id = $id;
                            $match->round = $loop + $j;
                            $match->home_team_id = $ids[$schedule[$j][$i][0] - 1];
                            $match->guest_team_id = $ids[$schedule[$j][$i][1] - 1];
                            $match->save();
                            $matches = $matches - 1;
                        } else if ($matches <= 0) {
                            return redirect('/admin/competitions/' . $id . '/matches');
                        }

                    }
                }
            }
            if ($n % 2 == 1) {
                $loop = $loop + $n;
            } else {
                $loop = $loop + $n - 1;
            }

        }
        return redirect('/admin/competitions/' . $id . '/matches');
    }

    public function GSchedule($schedule, $n)
    {
        $m = 0;
        if ($n % 2 == 0) {
            $m = $n;
        } else {
            $m = $n + 1;
        }

        $a = 1;
        $b = 1;
        $index = 1;
        $loop = 0;
        for ($i = 1; $i <= ($m - 1) * intval(($m / 2)); $i++) {
            if ($a >= $m) {
                $a = 1;
            }

            if ($index > intval($m / 2)) {
                $index = 1;
            }

            if ($index == 1) {
                $loop++;
                if ($i == 1) {
                    $b = $m;
                } else {
                    $b = $a;
                }
                if (intval(($i - 1) / intval($m / 2)) % 2 == 0) {
                    array_push($schedule[$loop], [$a, $m]);
                } else {
                    array_push($schedule[$loop], [$m, $a]);
                }
            } else if ($index > 1 && $index <= intval($m / 2)) {
                if ($b > 1) {
                    $b--;
                } else {
                    $b = $m - 1;
                }

                array_push($schedule[$loop], [$a, $b]);
            }
            $index++;
            $a++;
        }
        return $schedule;
    }

}
