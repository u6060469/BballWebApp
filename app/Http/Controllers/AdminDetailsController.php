<?php

namespace App\Http\Controllers;

use App\Competition;
use App\Detail;
use App\Http\Requests\DetailsRequest;
use Illuminate\Http\Request;

class AdminDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($competition_id)
    {
        $competition = Competition::findORFail($competition_id);
        return view('admin.competitions.details.index', compact('competition'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($competition_id)
    {
        $competition = Competition::findORFail($competition_id);
     return view('admin.competitions.details.create', compact('competition'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DetailsRequest $request)
    {
        $input = $request->all();
        Detail::create($input);
        return redirect('/admin/competitions/'.$input['competition_id'].'/details');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($competition_id, $id)
    {
        $competition = Competition::findORFail($competition_id);
        $detail = Detail::findOrFail($id);

        return view('admin.competitions.details.edit', compact('detail', 'competition'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DetailsRequest $request, $id, $competition_id)
    {
        $input = $request->all();
        $detail = Detail::findOrFail($id);
        $detail->update($input);
        return redirect('/admin/competitions/'.$competition_id.'/details');

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $competition_id)
    {
        $detail = Detail::findOrFail($id);
        $detail->delete();
        return redirect('/admin/competitions/'.$competition_id.'/details');

    }
}
