<?php

namespace App\Http\Controllers;

use App\Competition;
use App\Contact;
use App\Detail;
use App\Match;
use App\Player;
use App\Team;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PlayerCompetitionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $competitions = Competition::all();
        $now = Carbon::now();
        return view('welcome', compact('competitions', 'now'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $competition = Competition::findOrFail($id);
        $now = Carbon::now();
        $teams = Team::where('competition_id', $id)->pluck('name', 'id')->all();
        return view('player.competitions.join', compact('competition', 'teams', 'now'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $input['paid'] = 0;
        $canInsert = !(!!Player::where('name', $input['name'])->where('competition_id', $input['competition_id'])->first())
        && !(!!Player::where('email', $input['email'])->where('competition_id', $input['competition_id'])->first());
        if ($canInsert) {
            Player::create($input);
            flash('Thank you for joining, your request has been recorded')->success();
            return redirect('/competitions/' . $input['competition_id'] . '/teams');
        }
        flash('Sorry, this player has signed up for this competition.')->error();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $now = Carbon::now();
        $competition = Competition::findOrFail($id);
        $details = Detail::where('competition_id', $id)->get();
        return view('player.competitions.details', compact('competition', 'details', 'now'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showTeams($id)
    {
        $now = Carbon::now();
        $competition = Competition::findOrFail($id);
        $teams = Team::where('competition_id', $id)->get();
        $players = Player::where('competition_id', $id)->get();
        $count_player = 0;
        $count_team = 0;
        foreach ($teams as $team) {
            $query = Player::where('team_id', $team->id)->count();
            $count_player = $count_player > $query ? $count_player : $query;
            $count_team++;
        }
        return view('player.competitions.team', compact('competition', 'players', 'now', 'teams', 'count_player', 'count_team'));
    }

    public function showContact($id)
    {
        $now = Carbon::now();
        $competition = Competition::findOrFail($id);
        $contacts = Contact::where('competition_id', $id)->get();
        return view('player.competitions.contact', compact('competition', 'contacts', 'now'));
    }

    public function showDraws($id)
    {
        $now = Carbon::now();
        $competition = Competition::findOrFail($id);
        $teams = Team::where('competition_id', $id)->get();
        $matches = Match::where('competition_id', $id)->get();
        $draws = array();
        $ladders = array();
        foreach ($matches as $match) {
            $draw = (object) [
                'date' => $match->date,
                'time' => $match->time,
                'location' => $match->location,
                'home_team_name' => Team::findOrFail($match->home_team_id)->name,
                'home_team_score' => $match->home_team_score,
                'guest_team_name' => Team::findOrFail($match->guest_team_id)->name,
                'guest_team_score' => $match->guest_team_score,

            ];
            array_push($draws, $draw);
        }

        foreach ($teams as $team) {
            $GP = $this->findGP($team->id);
            $W = $this->findW($team->id);
            $D = $this->findD($team->id);
            $L = $this->findL($team->id);
            $GF = $this->findGF($team->id);
            $GA = $this->findGA($team->id);

            $ladder = (object) [
                'team_name' => $team->name,
                'team_id' => $team->id,
                'GP' => $GP,
                'W' => $W,
                'D' => $D,
                'L' => $L,
                'GF' => $GF,
                'GA' => $GA,
                'GD' => $GF - $GA,
                'PTS' => 2 * $W + $D,
            ];
            array_push($ladders, $ladder);
        }
        usort($ladders, array($this, "cmp"));
        return view('player.competitions.draws', compact('competition', 'draws', 'ladders', 'now'));
    }

    public function findGP($team_id)
    {
        return Match::where('home_team_id', $team_id)->whereNotNull('home_team_score')->count()
         + Match::where('guest_team_id', $team_id)->whereNotNull('guest_team_score')->count();
    }

    public function findW($team_id)
    {
        //As home
        $countAsHome = Match::whereRaw('guest_team_score < home_team_score')->where('home_team_id', $team_id)->get();
        //As guest
        $countAsGuest = Match::whereRaw('guest_team_score > home_team_score')->where('guest_team_id', $team_id)->get();
        return $countAsHome->count() + $countAsGuest->count();
    }

    public function findL($team_id)
    {
        //As home
        $countAsHome = Match::whereRaw('guest_team_score > home_team_score')->where('home_team_id', $team_id)->get();
        //As guest
        $countAsGuest = Match::whereRaw('guest_team_score < home_team_score')->where('guest_team_id', $team_id)->get();
        return $countAsHome->count() + $countAsGuest->count();
    }

    public function findGF($team_id)
    {
        //As home
        $countAsHome = Match::where('home_team_id', $team_id)->get();
        //As guest
        $countAsGuest = Match::where('guest_team_id', $team_id)->get();
        return $countAsHome->sum('home_team_score') + $countAsGuest->sum('guest_team_score');
    }

    public function findGA($team_id)
    {

        $countAsHome = Match::where('home_team_id', $team_id)->get();
        //As guest
        $countAsGuest = Match::where('guest_team_id', $team_id)->get();
        return $countAsHome->sum('guest_team_score') + $countAsGuest->sum('home_team_score');

    }

    public function findD($team_id)
    {
        //As home
        $countAsHome = Match::whereRaw('guest_team_score = home_team_score')->where('home_team_id', $team_id)->get();
        //As guest
        $countAsGuest = Match::whereRaw('guest_team_score = home_team_score')->where('guest_team_id', $team_id)->get();
        return $countAsHome->count() + $countAsGuest->count();
    }
    public function cmp($a, $b)
    {
        return strcmp($b->PTS, $a->PTS);
    }
}
