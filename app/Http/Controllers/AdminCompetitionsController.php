<?php

namespace App\Http\Controllers;

use App\Competition;
use App\Http\Requests\CompetitionsRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AdminCompetitionsController extends Controller
{

//    public function __construct()
//    {
//        $this->middleware('auth');
//    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $competitions = Competition::all();
        return view('admin.competitions.index', compact('competitions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function create()
    {
        $now = Carbon::now();
        return view('admin.competitions.create', compact('now'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompetitionsRequest $request)
    {

        Competition::create($request->all());
        return redirect('admin/competitions/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit_rego($id)
    {

        $competition = Competition::findOrFail($id);
        return view('admin.competitions.edit_rego', compact('competition'));
    }


    public function edit($id)
    {
        $competition = Competition::findOrFail($id);
        return view('admin.competitions.edit', compact('competition'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CompetitionsRequest $request, $id)
    {
        $input = $request->all();
        $competition = Competition::findOrFail($id);
        $competition->update($input);

        return redirect('/admin/competitions/'.$id.'/players');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Competition::findOrFail($id)->delete();
        return redirect('/admin/competitions');
    }
}
