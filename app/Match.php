<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    protected $fillable = [
        'competition_id',
        'round',
        'date',
        'time',
        'location',
        'home_team_id',
        'guest_team_id',
        'home_team_score',
        'guest_team_score'
    ];


    public function home_team(){
        return $this->belongsTo('App\Team', 'home_team_id');
    }

    public function guest_team(){
        return $this->belongsTo('App\Team', 'guest_team_id');
    }

}
