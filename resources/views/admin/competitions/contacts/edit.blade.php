@extends('layouts.admin')

@section('content')

    <br>
    <h3 class="float-left">{{ $competition->name}}/Contacts</h3>

    <div class="float-right">

    </div>
    <br>

    @include('admin.competitions.layouts.navbar')


    {!! Form::model($contact, ['method'=>'PATCH', 'action' =>['AdminContactsController@update', $contact->id, $competition->id], 'files' => true]) !!}

    <div class="form-group">
        {!! Form::label('name','Name') !!}
        {!! Form::text('name',null, ['class' =>'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('role','Role') !!}
        {!! Form::text('role',null, ['class' =>'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('mobile','Mobile') !!}
        {!! Form::text('mobile',null, ['class' =>'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('email','Email') !!}
        {!! Form::text('email',null, ['class' =>'form-control']) !!}
    </div>


    <div class="form-group">
        {!! Form::submit('Update contact', ['class'=>'btn btn-primary col-sm-6']) !!}
    </div>

    {!! Form::close() !!}



    {!! Form::open(['method'=>'DELETE', 'action' =>['AdminContactsController@destroy', $contact->id, $competition->id]]) !!}

    <div class="form-group">
        {!! Form::submit('Delete contact', ['class'=>'btn btn-danger col-sm-6']) !!}
    </div>

    {!! Form::close() !!}


    <div class="row">
        @include('includes.form_error')
    </div>



@stop