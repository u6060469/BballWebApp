
@extends('layouts.admin')

@section('content')

    <br>
    <h3 class="float-left">{{ $competition->name}}/Contacts</h3>

    <div class="float-right">
        <a href="{{ Request::url()}}/create" class="button btn btn-info"><i class="fa fa-plus-square"></i> Add a contact</a>
    </div>
    <br>

    @include('admin.competitions.layouts.navbar')






<div class="table-responsive">


        <table class="table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Role</th>
                <th>Mobile</th>
                <th>Email</th>

            </tr>
            </thead>
            <tbody>

            @if($competition->contacts)
                @foreach($competition->contacts as $contact)
            <tr>
                <td class="text-nowrap"><a href="{{ Request::url().'/'.$contact->id}}/edit">{{$contact->name}}</a></td>
                <td class="text-nowrap">{{$contact->role}}</td>
                <td class="text-nowrap">{{$contact->mobile}}</td>
                <td class="text-nowrap">{{$contact->email}}</td>
            </tr>
                @endforeach
                @endif
            </tbody>
        </table>


</div>


@stop