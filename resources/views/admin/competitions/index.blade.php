
@extends('layouts.admin')

@section('content')
    <br>

    <h3 class="float-left">Competitions</h3>


    <div class="float-right">

    <a href="{{route('users.index')}}" class="button btn btn-info"><i class="fas fa-user"></i> Users</a>
    <a href="{{route('competitions.create')}}" class="button btn btn-info"><i class="fa fa-plus-square"></i> Create a competition</a>

    </div>
    <div class="table-responsive">

        <table class="table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Registration close</th>

            </tr>
            </thead>
            <tbody>

            @if($competitions)
                @foreach($competitions as $competition)
                    <tr>
                        <td class="text-nowrap"><a href="{{ Request::url().'/'.$competition->id }}/players">{{$competition->name}}</a></td>
                        <td class="text-nowrap">{{$competition->rego_close->diffForhumans()}}</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>


    </div>


@stop