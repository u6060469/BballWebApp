@extends('layouts.admin')

@section('content')

    <br>
    <h3 class="float-left">{{ $competition->name}}/Teams</h3>

    <div class="float-right">

        <a class="button btn btn-info" href="{{ Request::url()}}/create"><i class="fa fa-plus-square"></i> Create a team</a>
        <div class="float-right ml-2">
            <allocation-table :competition_id="{{$competition->id}}"></allocation-table>
        </div>
    </div>
    <br>
    @include('admin.competitions.layouts.navbar')


            <table class="table">
                <thead>
                <tr>
                    <th>Names</th>
                </tr>
                </thead>
                <tbody>

                @if($competition->teams)
                    @foreach($competition->teams as $team)
                <tr>
                    <td class="text-nowrap"><a href="{{ Request::url().'/'.$team->id}}/edit">{{$team->name}}</a></td>
                </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>


@stop

