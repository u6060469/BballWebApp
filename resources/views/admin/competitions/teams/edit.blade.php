@extends('layouts.admin')

@section('content')
    <br>
    <h3 class="float-left">{{ $competition->name}}/Teams</h3>

    <div class="float-right">

    </div>
    <br>
    @include('admin.competitions.layouts.navbar')


    {!! Form::model($team, ['method'=>'PATCH', 'action' =>['AdminTeamsController@update', $team->id, $competition->id], 'files' => true]) !!}


    <div class="form-group">
        {{Form::label('name', 'Name')}}
        {{Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Title'])}}
    </div>

    <div class="form-group">
        {!! Form::submit('Update details', ['class'=>'btn btn-primary col-sm-6']) !!}
    </div>

    {!! Form::close() !!}


    {!! Form::open(['method'=>'DELETE', 'action' =>['AdminTeamsController@destroy', $team->id, $competition->id]]) !!}

    <div class="form-group">
        {!! Form::submit('Delete contact', ['class'=>'btn btn-danger col-sm-6']) !!}
    </div>

    {!! Form::close() !!}


{{-----------------------------------}}


    @if($team->players)
    <table class="table">
        <tbody>
        <thead>
        <tr>
            <th>Player name</th>
            <th>Player email</th>
        </tr>
        </thead>
        <tbody>
            @foreach($team->players as $player)
                <tr>
                    <td class="text-nowrap">{{$player->name}}</td>
                    <td class="text-nowrap">{{$player->email}}</td>

                </tr>
            @endforeach
        </tbody>
    </table>
    @endif
@stop