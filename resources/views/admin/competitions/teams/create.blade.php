@extends('layouts.admin')

@section('content')

    <br>
    <h3 class="float-left">{{ $competition->name}}/Teams</h3>

    <div class="float-right">

    </div>
    <br>
    @include('admin.competitions.layouts.navbar')


    {!! Form::open(['method'=>'POST', 'action' =>['AdminTeamsController@store', $competition->id ],'files' => true]) !!}

    <input type="hidden" value={{$competition->id}}  name="competition_id">

    <div class="form-group">
        {{Form::label('name', 'Name')}}
        {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Title'])}}
    </div>

    {{Form::submit('Create team', ['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}




<div class="row">
    @include('includes.form_error')
</div>


    @stop