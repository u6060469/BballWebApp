@extends('layouts.admin')

@section('content')<br>


<br>
<h3 class="float-left">{{ $competition->name}}/Edit registration dates</h3>

<div class="float-right">

</div>

@include('admin.competitions.layouts.navbar')




    {!! Form::model($competition, ['method'=>'PATCH', 'action' =>['AdminCompetitionsController@update', $competition->id], 'files' => true]) !!}


    <input type="hidden" id="name" name="name" value="{{$competition->name}}">


    <div class="form-group">
    {!! Form::label('rego_open','Registration open') !!}
    {!! Form::datetime('rego_open',null, ['class' =>'form-control']) !!}
    </div>


    <div class="form-group">
    {!! Form::label('rego_close','Registration close') !!}
    {!! Form::datetime('rego_close',null, ['class' =>'form-control']) !!}
    </div>


    <div class="form-group">
        {!! Form::submit('Update registration period', ['class'=>'btn btn-primary col-sm-6']) !!}
    </div>

    {!! Form::close() !!}




@stop