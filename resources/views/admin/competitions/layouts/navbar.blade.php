<br>
<br><br>

<ul class="nav nav-tabs nav-justified">

    <li class="nav-item">
        <a class="nav-link" href="{{route('players.index', $competition->id)}}">Players <span class="sr-only">(current)</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link"  href="{{route('teams.index', $competition->id)}}">Teams <span class="sr-only">(current)</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link"  href="{{route('matches.index', $competition->id)}}">Matches <span class="sr-only">(current)</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link"  href="{{route('details.index', $competition->id)}}">Details <span class="sr-only">(current)</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link"  href="{{route('contacts.index',$competition->id)}}">Contacts <span class="sr-only">(current)</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link"  href="{{route('competitions.edit_rego',$competition->id)}}">Registration<span class="sr-only">(current)</span></a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{route('competitions.edit', $competition->id)}}">Edit</a>
    </li>








</ul>

<br>
