{{--This currently on makes one match. I want it to be able to make multiple matches.--}}


@extends('layouts.admin')

@section('content')

    <br>
    <h3 class="float-left">{{ $competition->name}}/Matches</h3>

    <div class="float-right">

    </div>
    <br>

    @include('admin.competitions.layouts.navbar')


    {!! Form::open(['method'=>'POST', 'action' =>['AdminMatchesController@store', $competition->id ],'files' => true]) !!}

    <input type="hidden" value={{$competition->id}}  name="competition_id">


    <div class="form-group">
        {{Form::label('round', 'Round')}}
        {{Form::number('round', '', ['class' => 'form-control', 'placeholder' => ''])}}
    </div>

    <div class="form-group">
        {{Form::label('date', 'Date')}}
        {{Form::date('date', '', ['class' => 'form-control', 'placeholder' => ''])}}
    </div>

    <div class="form-group">
        {{Form::label('time', 'Time')}}
        {{Form::time('time', '', ['class' => 'form-control', 'placeholder' => ''])}}
    </div>

    <div class="form-group">
        {{Form::label('location', 'Location')}}
        {{Form::text('location', '', ['class' => 'form-control', 'placeholder' => ''])}}
    </div>


    <div class="form-group">
        {{Form::label('home_team', 'Home team')}}
        {!! Form::select('home_team_id', [''=>'Choose team']+$teams , 0, ['class' =>'form-control']) !!}
    </div>


    <div class="form-group">
        {{Form::label('away_team', 'Guest team')}}
        {!! Form::select('guest_team_id', [''=>'Choose team']+$teams , 0, ['class' =>'form-control']) !!}
    </div>


    {{Form::submit('Add match', ['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}

    <div class="row">
        @include('includes.form_error')
    </div>

@stop
