{{--Include the following statistics--}}
{{--E.g.--}}
{{--Current number of teams--}}
{{--How many matches it takes for every team to play every other team--}}
{{--How many matches within a round--}}

{{--Only input is number of matches--}}


@extends('layouts.admin')

@section('content')

    <br>
    <h3 class="float-left">{{ $competition->name}}/Matches</h3>

    <div class="float-right">

    </div>
    <br>

    @include('admin.competitions.layouts.navbar')

    @if($teams <= 1)
        <p>You need to add more teams</p>
        @else
    <p>There are {{$teams}} teams in your competition.

Every round will consist of {{($teams % 2 == 0)? ($teams/2).' matches.': ((floor($teams/2))).' matches with one team having a bye.'}}

 For each team to play every other team, you need
{{($teams % 2 == 0)? (($teams/2) * ($teams-1)).' matches.': (((($teams+1)/2) * ($teams))-$teams). ' matches with '.($teams). ' byes.' }}

        <br>
        <br>
        We recommend you select {{($teams % 2 == 0)? (($teams/2) * ($teams-1)).' matches.': (((($teams+1)/2) * ($teams))-$teams). ' matches' }}
    </p>




    {!! Form::open(['method'=>'POST', 'action' =>['AdminMatchesController@store_automatic', $competition->id ],'files' => true]) !!}

    <input type="hidden" value={{$competition->id}}  name="competition_id">


    <div class="form-group">
        {{Form::label('matches', 'Number of matches')}}
        {{Form::number('matches', '', ['class' => 'form-control', 'placeholder' => ''])}}
    </div>


    {{Form::submit('Automatically generate matches', ['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}

    <div class="row">
        @include('includes.form_error')
    </div>


    @endif
@stop

