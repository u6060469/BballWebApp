{{--Matches are sorted by round, then by date, then by time. --}}
{{--We need to be able to edit and update a match from this page, so this needs to be a form (not sure how to do
this--}}


@extends('layouts.admin')

@section('content')

 <br>
    <h3 class="float-left">{{ $competition->name}}/Matches</h3>

    <div class="float-right">
        <a href="{{Request::url()}}/create" class="button btn btn-info"><i class="fa fa-plus-square"></i> Add match</a>
        <a href="{{Request::url()}}/autocreate/{{$competition->id}}" class="button btn btn-info"><i class="fas fa-magic"></i>
            Automatic draw</a>
        <a href="{{Request::url()}}/clear/{{$competition->id}}" class="button btn btn-danger"><i class="fas fa-trash-alt"></i>
            Clear</a>
    </div>
    <br>

    @include('admin.competitions.layouts.navbar')





<div class="mt-5">
    <div class="row">
        <div class="col-1 text-center">Round</div>
        <div class="col-2 text-center">Date</div>
        <div class="col text-center">Time</div>
        <div class="col text-center">Location</div>
        <div class="col text-center">Home Team</div>
        <div class="col text-center">Score</div>
        <div class="col text-center ml-3">Guest Team</div>
        <div class="col text-center ml-3">Score</div>
        <div class="col text-center"></div>
        
    </div>
    <div>
        @if($matches)
            @foreach($matches as $match)
                {!! Form::open(['method'=>'PATCH','class' =>'form-row mt-3', 'action' =>['AdminMatchesController@update', $match->id,$competition->id],'files' => true]) !!}
                    {{Form::label('round', $match->round,['class' => 'col-1 text-center mr-1 pt-2'])}}
                    {{Form::date('date', $match->date, ['class' => 'form-control col-2 mr-1', 'placeholder' => ''])}}
                    {{Form::time('time', $match->time, ['class' => 'form-control col mr-1', 'placeholder' => ''])}}
                    {{Form::text('location', $match->location, ['class' => 'form-control col-1 mr-1', 'placeholder' => ''])}}
                    {{Form::select('home_team_id', [''=>$match->home_name]+$teams, $match->home_team_id, ['class' =>'form-control mr-1 col']) }}
                    {{Form::text('home_team_score', $match->home_team_score, ['class' => 'form-control mr-1 col', 'placeholder' => ''])}}
                    {!!Form::select('guest_team_id', [''=>$match->guest_name]+$teams, $match->guest_team_id, ['class'=>'form-control mr-1 col']) !!}
                    {{Form::text('guest_team_score', $match->guest_team_score, ['class' => 'form-control mr-1 col', 'placeholder' => ''])}}
                    {!!Form::submit("Update", ['class'=>'btn btn-default']) !!}

                <a href="{{Request::url()}}/destroy/{{$match->id}}" class="button btn btn-danger mr-1"><i class="fas fa-trash-alt"></i></a>

{{--                    <a href="" type="button" class="btn btn-danger" ><i class="fas fa-trash-alt"></i></a>--}}
                {!! Form::close() !!}
            @endforeach
        @endif
    </div>
</div>


    {{--</div>--}}



@stop
