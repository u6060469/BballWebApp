
@extends('layouts.admin')

@section('content')

    <br>
    <h3 class="float-left">{{ $competition->name}}/Players</h3>

    <div class="float-right">
        <a href="{{ Request::url()}}/create" class="button btn btn-info"><i class="fa fa-plus-square"></i>  Add a player</a>

        <a href="{{ Request::url()}}/export/{{$competition->id}}" class="button btn btn-info"><i class="fa fa-download"></i> Export player list</a>
        <div class="float-right ml-1">
                <allocation-table :competition_id="{{$competition->id}}"></allocation-table>
        </div>
    </div>
    <br>

    @include('admin.competitions.layouts.navbar')
    


<table class="table">
    <tbody>
    <thead>
    <tr>
        <th>Player name</th>
        <th>Player email</th>
        <th>Team</th>
        <th>Paid</th>
        <th></th>
    </tr>
    </thead>
    <tbody>

    @if($competition->players)
        @foreach($competition->players as $player)

    <tr>
        <td class="text-nowrap"><a href="{{ Request::url().'/'.$player->id}}/edit">{{$player->name}}</a></td>
        <td class="text-nowrap">{{$player->email}}</td>
        <td class="text-nowrap">


            {!! Form::open(['method'=>'PATCH', 'action' =>['AdminPlayersController@update', $player->id, $competition->id], 'files' => true]) !!}

            <div class="form-inline">
                <div class="form-group">
                {!! Form::select('team_id', [''=>'Choose team']+$teams , ($player->team ? $player->team->id: null), ['class' =>'form-control']) !!}
                </div>

                <div class="form-group">
                {!! Form::submit('Update', ['class'=>'btn btn-default']) !!}
                </div>
            </div>

            {!! Form::close() !!}

        </td>

        <td>
            @if($player->paid == 1)

                {!! Form::open(['method'=>'PATCH', 'action' =>['AdminPlayersController@update', $player->id, $competition->id], 'files' => true]) !!}

                <input type="hidden" name="paid" value="0">

                <div class="form-group">
                    <button type="submit" class="btn btn-success"><i class="fas fa-toggle-on"></i></button>

                </div>

                {!! Form::close() !!}

            @else

                {!! Form::open(['method'=>'PATCH', 'action' =>['AdminPlayersController@update', $player->id, $competition->id], 'files' => true]) !!}

                <input type="hidden" name="paid" value="1">

                <div class="form-group">
                    <button type="submit" class="btn btn-default"><i class="fas fa-toggle-off"></i></button>



                </div>

                {!! Form::close() !!}
            @endif

        </td>
    </tr>

        @endforeach
    @endif
    </tbody>
</table>






    @stop



{{------------------------------}}
