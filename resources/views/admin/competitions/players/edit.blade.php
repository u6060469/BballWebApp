@extends('layouts.admin')

@section('content')

    <br>
    <h3 class="float-left">{{ $competition->name}}/Players</h3>

    <div class="float-right">

    </div>
    <br>

    @include('admin.competitions.layouts.navbar')

    {!! Form::model($player, ['method'=>'PATCH', 'action' =>['AdminPlayersController@update', $player->id, $competition->id], 'files' => true]) !!}

    <div class="form-group">
        {{Form::label('name', 'Name')}}
        {{Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name'])}}
    </div>

    <div class="form-group">
        {{Form::label('email', 'Email')}}
        {{Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Email'])}}
    </div>

    <div class="form-group">
        {{Form::label('teammate', 'Teammate preferences')}}
        {{Form::text('teammate', null, ['class' => 'form-control', 'placeholder' => 'Teammate preferences'])}}
    </div>

    <div class="form-group">
        {{ Form::label('team_id', 'Team')}}
        {!! Form::select('team_id', [''=>'Choose team']+$teams , ($player->team ? $player->team->id: null), ['class' =>'form-control']) !!}
    </div>



    <div class="form-group">
        {!! Form::submit('Update players', ['class'=>'btn btn-primary col-sm-6']) !!}
    </div>

    {!! Form::close() !!}



    {!! Form::open(['method'=>'DELETE', 'action' =>['AdminPlayersController@destroy', $player->id, $competition->id]]) !!}

    <div class="form-group">
        {!! Form::submit('Delete contact', ['class'=>'btn btn-danger col-sm-6']) !!}
    </div>

    {!! Form::close() !!}



<div class="row">
    @include('includes.form_error')
</div>

@stop

