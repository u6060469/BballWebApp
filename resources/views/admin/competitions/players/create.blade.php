
@extends('layouts.admin')

@section('content')

    <br>
    <h3 class="float-left">{{ $competition->name}}/Players</h3>

    <div class="float-right">

    </div>
    <br>

    @include('admin.competitions.layouts.navbar')


    {!! Form::open(['method'=>'POST', 'action' =>['AdminPlayersController@store', $competition->id ],'files' => true]) !!}

    <input type="hidden" value={{$competition->id}} name="competition_id">

    <div class="form-group">
        {{Form::label('name', 'Name')}}
        {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Name'])}}
    </div>

    <div class="form-group">
        {{Form::label('email', 'Email')}}
        {{Form::text('email', '', ['class' => 'form-control', 'placeholder' => 'Email'])}}
    </div>

    <div class="form-group">
        {{Form::label('teammate', 'Teammate preferences')}}
        {{Form::text('teammate', '', ['class' => 'form-control', 'placeholder' => 'Teammate preferences'])}}
    </div>

    <div class="form-group">
        {{Form::label('team_id', 'Team')}}
        {!! Form::select('team_id', [''=>'Choose team']+$teams , 0, ['class' =>'form-control']) !!}
    </div>


    {{Form::submit('Create player', ['class'=>'btn btn-primary'])}}

    {!! Form::close() !!}


<div class="row">
    @include('includes.form_error')
</div>


@stop