@extends('layouts.admin')

@section('content')

    <br>
    <h3 class="float-left">{{ $competition->name}}/Edit or delete competition</h3>

    <div class="float-right">
        <a href="{{ Request::url()}}/create" class="button btn btn-info"><i class="fa fa-plus-square"></i>  Add a player</a>

    </div>



    @include('admin.competitions.layouts.navbar')




    {!! Form::model($competition, ['method'=>'PATCH', 'action' =>['AdminCompetitionsController@update', $competition->id], 'files' => true]) !!}

    <div class="form-group">
        {!! Form::label('name','Name') !!}
        {!! Form::text('name',null, ['class' =>'form-control']) !!}
    </div>

    {{--<div class="form-group">--}}
        {{--{!! Form::label('rego_open','Registration open') !!}--}}
        {{--{!! Form::datetime('rego_open',null, ['class' =>'form-control']) !!}--}}
    {{--</div>--}}


    {{--<div class="form-group">--}}
        {{--{!! Form::label('rego_close','Registration close') !!}--}}
        {{--{!! Form::datetime('rego_close',null, ['class' =>'form-control']) !!}--}}
    {{--</div>--}}



    <div class="form-group">
        {!! Form::submit('Update contact', ['class'=>'btn btn-primary col-sm-6']) !!}
    </div>

    {!! Form::close() !!}



    {!! Form::open(['method'=>'DELETE', 'action' =>['AdminCompetitionsController@destroy', $competition->id]]) !!}

    <div class="form-group">
        {!! Form::submit('Delete contact', ['class'=>'btn btn-danger col-sm-6']) !!}
    </div>

    {!! Form::close() !!}



    <div class="row">
        @include('includes.form_error')
    </div>


@stop