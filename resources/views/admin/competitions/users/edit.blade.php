
@extends('layouts.admin')

@section('content')

    <h1>{{$user->name}}</h1>


            {!! Form::model($user, ['method'=>'PATCH', 'action' =>['AdminUsersController@update', $user->id], 'files' => true]) !!}

            {{--<div class="form-group">--}}
                {{--{!! Form::label('name','Name:') !!}--}}
                {{--{!! Form::text('name',null, ['class' =>'form-control']) !!}--}}
            {{--</div>--}}

            {{--<div class="form-group">--}}
                {{--{!! Form::label('email','Email:') !!}--}}
                {{--{!! Form::email('email',null, ['class' =>'form-control']) !!}--}}
            {{--</div>--}}

            <div class="form-group">
                {!! Form::label('is_admin','Status:') !!}
                {!! Form::select('is_admin',array(0 => 'Not Admin',1=>'Admin' ), null, ['class' =>'form-control']) !!}
            </div>


            {{--<div class="form-group">--}}
                {{--{!! Form::label('password','Password:') !!}--}}
                {{--{!! Form::password('password', ['class' =>'form-control']) !!}--}}
            {{--</div>--}}


            <div class="form-group">
                {!! Form::submit('Update user', ['class'=>'btn btn-primary col-sm-6']) !!}
            </div>

            {!! Form::close() !!}


            {!! Form::open(['method'=>'DELETE', 'action' =>['AdminUsersController@destroy', $user->id]]) !!}

            <div class="form-group">
                {!! Form::submit('Delete user', ['class'=>'btn btn-danger col-sm-6']) !!}
            </div>

    {!! Form::close() !!}


    <div class="row">
        @include('includes.form_error')
    </div>

@stop