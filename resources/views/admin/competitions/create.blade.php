
@extends('layouts.admin')

@section('content')
<br>

<h3>Create a competition</h3>


    {!! Form::open(['method'=>'POST', 'action' =>'AdminCompetitionsController@store', 'files' => true]) !!}

    <div class="form-group">
        {!! Form::label('name','Name') !!}
        {!! Form::text('name',null, ['class' =>'form-control']) !!}
    </div>



    <input type="hidden" id="rego_open" name="rego_open" value="{{$now}}">
    <input type="hidden" id="rego_close" name="rego_close" value="{{$now}}">



    <div class="form-group">
        {!! Form::submit('Create competition', ['class'=>'btn btn-primary col-sm-6']) !!}
    </div>

    {!! Form::close() !!}


    <div class="row">
        @include('includes.form_error')
    </div>

@stop