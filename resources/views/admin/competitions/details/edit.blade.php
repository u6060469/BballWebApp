@extends('layouts.admin')

@section('content')

    <br>
    <h3 class="float-left">{{ $competition->name}}/Details</h3>

    <div class="float-right">

    </div>
    <br>

    @include('admin.competitions.layouts.navbar')


    {!! Form::model($detail, ['method'=>'PATCH', 'action' =>['AdminDetailsController@update', $detail->id, $competition->id], 'files' => true]) !!}


    <div class="form-group">
        {{Form::label('title', 'Title')}}
        {{Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title'])}}
    </div>
    <div class="form-group">
        {{Form::label('content', 'Body')}}
        {{ Form::textarea('content', null, ['id' => 'editor', 'class' => 'form-control']) }}
    </div>


    <div class="form-group">
        {!! Form::submit('Update details', ['class'=>'btn btn-primary col-sm-6']) !!}
    </div>

    {!! Form::close() !!}



    {!! Form::open(['method'=>'DELETE', 'action' =>['AdminDetailsController@destroy', $detail->id, $competition->id]]) !!}

    <div class="form-group">
        {!! Form::submit('Delete contact', ['class'=>'btn btn-danger col-sm-6']) !!}
    </div>

    {!! Form::close() !!}


@section('js')
    <script> CKEDITOR.replace('editor'); </script>
@endsection


<div class="row">
    @include('includes.form_error')
</div>

@stop



