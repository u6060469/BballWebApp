
@extends('layouts.admin')

@section('content')

    <br>
    <h3 class="float-left">{{ $competition->name}}/Details</h3>

    <div class="float-right">

    </div>
    <br>

    @include('admin.competitions.layouts.navbar')


    {!! Form::open(['method'=>'POST', 'action' =>['AdminDetailsController@store', $competition->id ],'files' => true]) !!}

<input type="hidden" value={{$competition->id}}  name="competition_id">


   <div class="form-group">
        {{Form::label('title', 'Title')}}
        {{Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Title'])}}
    </div>
    <div class="form-group">
        {{Form::label('content', 'Body')}}
        {{ Form::textarea('content', '', ['id' => 'editor', 'class' => 'form-control']) }}
    </div>

    {{Form::submit('Submit', ['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}


    @section('js')
    <script> CKEDITOR.replace('editor'); </script>
    @endsection


    <div class="row">
        @include('includes.form_error')
    </div>


@stop