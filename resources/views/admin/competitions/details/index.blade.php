
@extends('layouts.admin')

@section('content')

    <br>
    <h3 class="float-left">{{ $competition->name}}/Details</h3>

    <div class="float-right">

        <a href="{{ Request::url()}}/create" class="button btn btn-info"><i class="fa fa-plus-square"></i> Add competition detail</a>

    </div>
    <br>

    @include('admin.competitions.layouts.navbar')




<div class="table-responsive">


        <table class="table">
            <thead>
            <tr>
                <th>Title</th>

            </tr>
            </thead>
            <tbody>

            @if($competition->details)
                @foreach($competition->details as $detail)
            <tr>
                <td class="text-nowrap"><a href="{{ Request::url().'/'.$detail->id}}/edit">{{$detail->title}}</a></td>

            </tr>
                @endforeach
                @endif
            </tbody>
        </table>


</div>


@stop