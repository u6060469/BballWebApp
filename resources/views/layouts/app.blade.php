<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Bball') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>




<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
    <a class="navbar-brand" href="/">All competitions</a>
        @if(Route::currentRouteName() != 'home')
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">

        <ul class="navbar-nav mr-auto">



                <li class="nav-item">
                        <a class="nav-link"  href="/competitions/{{$competition->id}}">
                            Details
                        </a>
                    </li>
                <li class="nav-item">
                        <a class="nav-link"  href="/competitions/{{$competition->id}}/teams">
                            Teams
                        </a>
                    </li>
                <li class="nav-item">
                        <a class="nav-link"  href="/competitions/{{$competition->id}}/draws">
                            Matches
                        </a>
                    </li>


            @if($now < $competition->rego_close && $now > $competition->rego_open)
                <li class="nav-item">
                        <a  class="nav-link" href="/competitions/{{$competition->id}}/join">
                            Join competition
                        </a>
                    </li>
            @endif
                <li class="nav-item">
                        <a class="nav-link"  href="/competitions/{{$competition->id}}/contact">
                            Contacts
                        </a>
                    </li>
            </ul>


    </div>
        @endif


        @guest
        @else
            <ul class="navbar-nav mr-auto">
        <li class="nav-item dropdown">
            <a id="navbarDropdown"
               class="nav-link dropdown-toggle"
               href="#"
               role="button"
               data-toggle="dropdown"
               aria-haspopup="true"
               aria-expanded="false"
               onclick="
                                    if (document.getElementById('logout-dropdown').style.display!='block') {
                                        document.getElementById('logout-dropdown').style.display ='block'
                                    } else {
                                        document.getElementById('logout-dropdown').style.display = 'none'
                                    }
                                ">
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" id="logout-dropdown">
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
            </ul>
            @endguest
        </div>



</nav>



















        {{--<div class="sidebar text-white">--}}
            {{--<div class="sidebar-header">--}}
                {{--<a href="/">--}}


                   {{--<img src="{{ url('storage/img/logo.png') }}">--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<ul class="sidebar-content">--}}
                {{--@if(Route::currentRouteName() != 'home')--}}
                    {{--<li class="sidebar-item">--}}
                        {{--<a href="/">--}}
                            {{--All Competitions--}}
                        {{--</a>--}}
                    {{--</li>--}}

                    {{--<li class="sidebar-item">--}}
                        {{--<a href="/competitions/{{$competition->id}}">--}}
                            {{--Details--}}
                        {{--</a>--}}
                    {{--</li>--}}
                    {{--<li class="sidebar-item">--}}
                        {{--<a href="/competitions/{{$competition->id}}/teams">--}}
                            {{--Teams--}}
                        {{--</a>--}}
                    {{--</li>--}}
                    {{--<li class="sidebar-item">--}}
                        {{--<a href="/competitions/{{$competition->id}}/draws">--}}
                            {{--Draw and results--}}
                        {{--</a>--}}
                    {{--</li>--}}
                    {{--<li class="sidebar-item">--}}
                        {{--<a href="/competitions/{{$competition->id}}/join">--}}
                            {{--Join competition--}}
                        {{--</a>--}}
                    {{--</li>--}}
                    {{--<li class="sidebar-item">--}}
                        {{--<a href="/competitions/{{$competition->id}}/contact">--}}
                            {{--Contacts--}}
                        {{--</a>--}}
                    {{--</li>--}}
                {{--@endif--}}

            {{--</ul>--}}

        {{--</div>--}}
        <div class="container">
            @include('flash::message')
            @yield('content')
        </div>
    </div>
</body>
</html>
