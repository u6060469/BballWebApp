<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->

    <script src="{{ asset('js/app.js') }}"></script>



    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">






</head>
<body>





<div id="app">
    <nav class="navbar navbar-expand-lg navbar-light bg-dark">
        <div class="container">

            {{--<a class="navbar-brand text-white" href="{{ url('/') }}">--}}
                {{--{{ config('app.name', 'competition manager') }}--}}
            {{--</a>--}}

            <a class="navbar-brand text-white nav-item lead" href="{{ url('/admin/competitions') }}">Competitions
                <i class="fas fa-list-ol"></i>
            </a>

            {{--<button class="navbar-toggler" style="background:white;border:none" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">--}}
                {{--<span class="navbar-toggler-icon"></span>--}}
            {{--</button>--}}

            {{--<div class="collapse navbar-collapse" id="navbarSupportedContent">--}}
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" 
                                class="nav-link dropdown-toggle text-white" 
                                href="#" 
                                role="button" 
                                data-toggle="dropdown" 
                                aria-haspopup="true" 
                                aria-expanded="false"
                                onclick="
                                    if (document.getElementById('logout-dropdown').style.display!='block') {
                                        document.getElementById('logout-dropdown').style.display ='block'
                                    } else {
                                        document.getElementById('logout-dropdown').style.display = 'none'
                                    }
                                ">  
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" id="logout-dropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            {{--</div>--}}
        </div>
    </nav>




<div class="container bg-light">
        <div class="offset-lg-1 mt-3">
            @include('flash::message')
        </div>
        
        @yield('content')
</div>

</div>




<script src="{{ asset('js/app.js') }}"></script>
{{--^4.7--}}
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>




@yield('js')
</body>
</html>
