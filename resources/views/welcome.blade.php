
@extends('layouts.app')

@section('content')

<h1>Competitions</h1>
<table class="table table-hover">
    <thead>
        <tr>
            <th>Competition name</th>
            <th>Registration status</th>
            <th>Registration Open</th>
            <th>Registration Close</th>
        </tr>
    </thead>
    <tbody>
        @foreach($competitions as $competition)
                <tr>
                    <td>
                        <a href="/competitions/{{$competition->id}}">
                            {{$competition->name}}
                        </a>
                    </td>
                    <td>
                        @if($now < $competition->rego_close && $now > $competition->rego_open)
                            Open
                            @else
                            Closed
                        @endif
                    </td>
                    <td>{{$competition->rego_open->diffForhumans()}}</td>
                    <td>{{$competition->rego_close->diffForhumans()}}</td>
                </tr>

        @endforeach
    </tbody>
</table>





{{--<h1 style="margin-top:3rem">Unavailable Competitions</h1>--}}
{{--<table class="table table-hover">--}}
    {{--<thead>--}}
        {{--<tr>--}}
            {{--<th>Names</th>--}}
            {{--<th>Registration Open</th>--}}
            {{--<th>Registration Close</th>--}}
        {{--</tr>--}}
    {{--</thead>--}}
    {{--<tbody>--}}
        {{--@foreach($competitions as $competition)--}}
            {{--@if($now > $competition->rego_close || $now < $competition->rego_open)--}}
                {{--<tr>--}}
                    {{--<td>--}}
                        {{--<a href="/competitions/{{$competition->id}}">--}}
                            {{--{{$competition->name}}--}}
                        {{--</a>--}}
                    {{--</td>--}}
                    {{--<td>{{$competition->rego_open->diffForhumans()}}</td>--}}
                    {{--<td>{{$competition->rego_close->diffForhumans()}}</td>--}}


                {{--</tr>--}}
            {{--@endif--}}
        {{--@endforeach--}}
    {{--</tbody>--}}
{{--</table>--}}

@endsection