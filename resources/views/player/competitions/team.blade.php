@extends('layouts.app')

@section('content')
    @if((count($teams)) > 0)
    <h1>Teams for {{$competition->name}}</h1>
    <div class="col-12">
        <table class="table text-center">
            <tr>
                <th colspan="{{$count_team}}">Teams</th>
            </tr>
            <tr>
                @foreach ($teams as $team)
                    <th>
                        <table class="table-bordered">
                            <tr>
                                <th>{{$team->name}}</th>
                            </tr>
                            @foreach ($players as $player)
                                @if($player->team_id == $team->id)
                                    @if($player->paid == 0)
                                    <tr>
                                        <td class="list-group-item-danger">{{$player->name}}</td>
                                    </tr>
                                    @else
                                    <tr>
                                        <td>{{$player->name}}</td>
                                    </tr>
                                    @endif
                                @endif
                            @endforeach
                        </table>        
                    </th>
                @endforeach
            </tr>
            <tr>
            </tr>
        </table>
    </div>

    @else
       No teams added for '{{$competition->name}}'
    @endif
@endsection