@extends('layouts.app')

@section('content')

    @if($now > $competition->rego_close || $now < $competition->rego_open)
        <p class="btn-danger">Registration is closed</p>
    @endif

    @if((count($details)) > 0)
    <h1 class="mb-5">{{$competition->name}}</h1>



    @foreach ($details as $detail)
        <h2>{{$detail->title}}</h2>
        <p>{!!$detail->content!!}</p>
    @endforeach
    @else
    No details added for '{{$competition->name}}'
    @endif
@endsection