@extends('layouts.app')

@section('content')



    @if((count($contacts)) > 0)

    <h1>Contacts for {{$competition->name}}</h1>
    <div class="col-12">
        <table class="table table-bordered text-center">
            <thead>
                <tr>
                    <th>name</th>
                    <th>role</th>
                    <th>phone number</th>
                    <th>email</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($contacts as $contact)
                    <tr>
                        <td>{{$contact->name}}</td>
                        <td>{{$contact->role}}</td>
                        <td>{{$contact->mobile}}</td>
                        <td>
                            <a href="mailto:{{$contact->email}}">
                                {{$contact->email}}
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    @else
        No contacts added for '{{$competition->name}}'
    @endif
    
@endsection