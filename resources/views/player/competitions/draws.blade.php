@extends('layouts.app')

@section('content')

    @if((count($ladders)) > 0)
    <h1>Ladders</h1>
    <div class="col-12">
        <table class="table table-bordered text-center">
            <thead>
                <tr>
                    <th></th>
                    <th>GP</th>
                    <th>W</th>
                    <th>D</th>
                    <th>L</th>
                    <th>GF</th>
                    <th>GA</th>
                    <th>GD</th>
                    <th>PTS</th>
                </tr>
            </thead>
            @foreach ($ladders as $ladder)
                <tr>
                    <th>{{$ladder->team_name}}</th>
                    <th>{{$ladder->GP}}</th>
                    <th>{{$ladder->W}}</th>
                    <th>{{$ladder->D}}</th>
                    <th>{{$ladder->L}}</th>
                    <th>{{$ladder->GF}}</th>
                    <th>{{$ladder->GA}}</th>
                    <th>{{$ladder->GD}}</th>
                    <th>{{$ladder->PTS}}</th>
                </tr>
            @endforeach
        </table>
    </div>

    @if((count($draws)) > 0)

    <h1>Draw</h1>
    <div class="col-12">
        <table class="table table-bordered text-center">
            <thead>
                <tr>
                    <td>Date</td>
                    <td>Start time</td>
                    <td>Location</td>
                    <td>Home team</td>
                    <td>Points</td>
                    <td>Away team</td>
                    <td>Points</td>
                </tr>
            </thead>
            <tbody>
                @foreach ($draws as $draw)
                    <tr>
                        <td>{{$draw->date}}</td>
                        <td>{{$draw->time}}</td>
                        <td>{{$draw->location}}</td>
                        <td>{{$draw->home_team_name}}</td>
                        <td>{{$draw->home_team_score}}</td>
                        <td>{{$draw->guest_team_name}}</td>
                        <td>{{$draw->guest_team_score}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
        @endif
    @else
    No matches added for '{{$competition->name}}'
    @endif
@endsection