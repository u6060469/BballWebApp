@extends('layouts.app')

@section('content')
    @if($now < $competition->rego_close && $now > $competition->rego_open)

    <h1>Join {{$competition->name}}</h1>
    <div class="col-10">
        {!! Form::open(['method'=>'POST', 'action' =>['PlayerCompetitionsController@store', $competition->id ],'files' => true]) !!}

            <input type="hidden" value={{$competition->id}} name="competition_id">
            <div class="form-group">
                {{Form::label('name', 'Name')}}
                {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Name'])}}
            </div>
            <div class="form-group">
                {{Form::label('email', 'Email')}}
                {{Form::text('email', '', ['class' => 'form-control', 'placeholder' => 'Email'])}}
            </div>
            <div class="form-group">
                {{Form::label('teammate', 'Teammate preferences')}}
                {{Form::text('teammate', '', ['class' => 'form-control', 'placeholder' => 'Teammate preferences'])}}
            </div>
    
            <div class="form-group">
                {{Form::label('team_id', 'Team')}}
                {!! Form::select('team_id', [''=>'Choose team']+$teams , 0, ['class' =>'form-control']) !!}
            </div>
    
            {{Form::submit('Join now', ['class'=>'btn btn-primary form-control'])}}
        {!! Form::close() !!}
    </div>



    <div class="row">
        @include('includes.form_error')
    </div>

    @else
        Registration closed for '{{$competition->name}}'
    @endif
@endsection