<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', ['as' => 'home', 'uses' => 'PlayerCompetitionsController@index']);
Route::get('/competitions/{id}', ['as' => 'competition_field', 'uses' => 'PlayerCompetitionsController@show']);
Route::get('/competitions/{id}/teams', 'PlayerCompetitionsController@showTeams');
Route::get('/competitions/{id}/join', 'PlayerCompetitionsController@create');
Route::get('/competitions/{id}/contact', 'PlayerCompetitionsController@showContact');
Route::get('/competitions/{id}/draws','PlayerCompetitionsController@showDraws');
Route::post('/competitions/join', 'PlayerCompetitionsController@store');

Auth::routes();

Route::get('/admin', function () {
    return redirect('login');
});

Route::group(['prefix' => 'admin', 'middleware' => ['admin', 'web', 'auth']], function () {

    Route::resource('competitions', 'AdminCompetitionsController');
    Route::resource('users', 'AdminUsersController');
    Route::get('competitions/{competition_id}/edit_rego', 'AdminCompetitionsController@edit_rego')->name('competitions.edit_rego');

    // !!! DIFFERENT ADMIN PANEL FOR USER POSTS
    Route::group(['prefix' => 'competitions/{competition_id}'], function () {
        Route::resource('contacts', 'AdminContactsController');
        Route::resource('details', 'AdminDetailsController');
        Route::resource('players', 'AdminPlayersController');
        Route::resource('teams', 'AdminTeamsController');
        Route::resource('matches', 'AdminMatchesController');
        Route::get('players/export/{id}', 'ExcelController@export');
        Route::post('matches/store_automatic', 'AdminMatchesController@store_automatic');
        Route::get('matches/autocreate/{id}', 'AdminMatchesController@autoCreate');
        Route::get('matches/clear/{id}', 'AdminMatchesController@clear');
        Route::get('matches/destroy/{match_id}', 'AdminMatchesController@destroy');

    });
});
