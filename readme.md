# UNI COMP SHACK

# Description
UNI COMP SHACK is a sport competition management web application. We designed the application specifically for ANU university students managing and playing in social sport competitions on campus.
The design takes into consideration the high turn-over of volunteer students in competition management roles by focusing on a intuitive, simplistic design that does not have a large learning curve. The design also takes into consideration that student volunteers are time-poor and so the application endeavours to automate essential tasks needed to run a competition but not any further features. 
Although we considered many different users (competition manager, referee manager, referees, team managers, players, scorers), we decided for the sake of simplicity to focus on two, namely, competition manager and players.
The main tasks that the application automates or simplifies is registration of players, communication between players and competition manager, draw generation, and inputting of results. 
The application has views that are publicly available for all (players, competition managers, etc), and an administrator backend for competition managers only (behind a login), where they can created and manage their competitions.   

The initial purpose of this project was to build a web application to supersede the competition management part of [ANU Basketball Club Website](http://anubasketballclub.com/internal_about.php), 
which used google forms and sheets for registration and displaying: team lists, draws, and results. 

See [elevator pitch](.documents-sem1-2018/elevator-pitch.pdf), [business model pitch](.documents-sem1-2018/business-model-pitch.pdf), and also [business model canvas](.documents-sem1-2018/business-model-canvas.pdf)

Team: Zhongyan Lou (front-end), Noah Johnstone (client, project manager), Rui Zheng (front-end), Shanbin Wang (front-end), Zixuan Zheng (back-end), Zihao Liu (back-end).
Mentor: David Heacock.

# Features

As a competition manager, I can:

* [Register for admin and be given admin access by another admin](https://gitlab.cecs.anu.edu.au/u6060469/BballWebApp/blob/master/.documents-sem2-2018/flow_maps/create_admin_account.pdf)
* Log in as admin
* Give access to other registered users to manage the competitions
* [Create a competition](https://gitlab.cecs.anu.edu.au/u6060469/BballWebApp/blob/master/.documents-sem2-2018/flow_maps/create_competition.pdf)
* [Edit a competition name](https://gitlab.cecs.anu.edu.au/u6060469/BballWebApp/blob/master/.documents-sem2-2018/flow_maps/change_competition_name.pdf)
* Delete a competition
* [Edit default registration period](https://gitlab.cecs.anu.edu.au/u6060469/BballWebApp/blob/master/.documents-sem2-2018/flow_maps/change_registration_period.pdf)
* [Create a team](https://gitlab.cecs.anu.edu.au/u6060469/BballWebApp/blob/master/.documents-sem2-2018/flow_maps/create_teams.pdf)
* Edit a team's name
* Delete a team
* See a player entry that has submited a registration form
* Create a player entry
* Edit player information
* Delete a player
* [Make a note if a players has paid for competition (i.e. they have met all the requirements to play (e.g. paid their competition fees))](https://gitlab.cecs.anu.edu.au/u6060469/BballWebApp/blob/master/.documents-sem2-2018/flow_maps/note_player_has_paid.pdf)
* [Export list of players with their: email, team and whether they have paid or not](https://gitlab.cecs.anu.edu.au/u6060469/BballWebApp/blob/master/.documents-sem2-2018/flow_maps/export_player_contacts.pdf)
* [assign player in to a team](https://gitlab.cecs.anu.edu.au/u6060469/BballWebApp/blob/master/.documents-sem2-2018/flow_maps/assign_player_to_team.pdf)
* Assign many players into teams
* [Create one match](https://gitlab.cecs.anu.edu.au/u6060469/BballWebApp/blob/master/.documents-sem2-2018/flow_maps/create_one_match.pdf)
* Edit match details (e.g. date, time, location, teams in match)
* Delete one match
* Delete all matches
* [Add multiple matches using an automatic draw](https://gitlab.cecs.anu.edu.au/u6060469/BballWebApp/blob/master/.documents-sem2-2018/flow_maps/create_automatic_draw.pdf)
* [Input scores for a match](https://gitlab.cecs.anu.edu.au/u6060469/BballWebApp/blob/master/.documents-sem2-2018/flow_maps/input_scores_for_a_match.pdf)
* [Create information about a competition](https://gitlab.cecs.anu.edu.au/u6060469/BballWebApp/blob/master/.documents-sem2-2018/flow_maps/create_information_about_competition.pdf)
* Edit information about a competition
* Delete information about a competition
* [Create a competition contact](https://gitlab.cecs.anu.edu.au/u6060469/BballWebApp/blob/master/.documents-sem2-2018/flow_maps/create_contact.pdf)
* Edit a competition contact
* Delete a competition contact


As a player, I can:

* See a list of current competition
* See whether the competition registration is open
* See information about the competition that the competition manager has added
* Find out who they can contact if they want to ask further questions about the competition
* Join a team
* See the team I am in
* See whether I have paid or not
* See the draw
* See results and the ladder
* [All flows for players here](https://gitlab.cecs.anu.edu.au/u6060469/BballWebApp/blob/master/.documents-sem2-2018/flow_maps/Presentation1.pdf)



# Light weight documentation
* [Meeting minutes](https://gitlab.cecs.anu.edu.au/u6060469/BballWebApp/wikis/Meeting-minutes)
* [Decision log](https://gitlab.cecs.anu.edu.au/u6060469/BballWebApp/wikis/Decision-Log)
* [Reference material](https://gitlab.cecs.anu.edu.au/u6060469/BballWebApp/wikis/Reference-material)


# Work flow
[Work flow](https://gitlab.cecs.anu.edu.au/u6060469/BballWebApp/blob/master/.documents-sem2-2018/workflow.pdf)

# User research
|Type|Link|
|:---:|:---:|
|User research/suggested improvements added into meeting minutes|[Meeting minutes](https://gitlab.cecs.anu.edu.au/u6060469/BballWebApp/wikis/Meeting-minutes)|
|User research|[25-Aug-2018](https://gitlab.cecs.anu.edu.au/u6060469/BballWebApp/blob/master/.documents-sem2-2018/UX0825%20.pdf)|
|User research|[30-Aug-2018](https://gitlab.cecs.anu.edu.au/u6060469/BballWebApp/blob/master/.documents-sem2-2018/UX_0830.pdf)|
|User research (player)|[9-Sep-2018](https://gitlab.cecs.anu.edu.au/u6060469/BballWebApp/blob/master/.documents-sem2-2018/UX_player_910.pdf)|

# Wireframe/Prototypes/MVPS
|Type|Link|
|:---:|:---:|
|Wireframe|[matches](https://gitlab.cecs.anu.edu.au/u6060469/BballWebApp/blob/master/.documents-sem2-2018/Results%20and%20draw.pdf)|
|Wireframe|[competition manager](.documents-sem1-2018/wireframe-comp-manager.png)|
|Wireframe|[fragement or draw](.documents-sem1-2018/wireframe-com-manager-fragment.png)|
|Wireframe|[player](.documents-sem1-2018/wireframe-player.pdf)|
|Prototype 0|[player](http://alicenbecca.com/BballWebAppPlayer/competitions.php)|
|Prototype 0|[competition manager](http://alicenbecca.com/BballWebAppPrototype/competitions.php)|
|Prototype 1|[competition manager/player](http://alicenbecca.com/prototype1/)|
|Prototype 2|[competition manager/player](http://alicenbecca.com/prototype2/)|
|Prototype 3|[competition manager/player](http://alicenbecca.com/prototype3/)|
|ER diaggram| [ER diagram 25/8/2019] (https://gitlab.cecs.anu.edu.au/u6060469/BballWebApp/blob/master/.documents-sem2-2018/ER-diagram.pdf) |
|Outdated Deployment - Player side|[player](http://alicenbecca.com/)|
|Outdated Deployment - competition manager side|[competition manager](http://alicenbecca.com/login)|

# Tools
* Gitlab
* Laravel
* Bootrap 4
* Vue.js
* MySQl
* Slack
* [Technical specification on the player's side](https://gitlab.cecs.anu.edu.au/u6060469/BballWebApp/blob/master/.documents-sem2-2018/Technical_specification_on_the_players_side.md)

# Future of project
* Open source the project
* Deploy the project through  ITS TechLauncher Host.
* Give a competition manager from Basketball Club or another ANU Sport Club admin access to the web application so they can run a competition. 

# Reflection for the year
* [Zhongyan Lou(Steve)](https://gitlab.cecs.anu.edu.au/u6060469/BballWebApp/blob/master/.documents-sem2-2018/steve_reflection.md)		
* [Rui Zheng(Rick)](https://gitlab.cecs.anu.edu.au/u6060469/BballWebApp/blob/master/.documents-sem2-2018/RickReflection.md)  
* Shanbin Wang(Oliver):
* Zixuan Zheng(John):	
I think through this year's experience, we grow up a project from some ideas to the product which can really helps the players of ANU Basketball to manage their competitions and career, which is super exceited. I am very happy and proud that I made some contribution to the community of ANU. Besides, my code skills at php and vue.js also improved which would help me in the future.
* Zihao liu(Charles):After a year of hard work, we turned the client's ideas into a usable product. During the year, I learned how to communicate with others, and I also learned how to develop web applications. This is a very meaningful experience. And I am very happy that the product can be deployed.	
* [Noah Johnstone(Macca)](https://gitlab.cecs.anu.edu.au/u6060469/BballWebApp/blob/master/.documents-sem2-2018/noah_reflection.md):	

# Deployment
[Deployment](https://devmarketer.io/learn/deploy-laravel-5-app-lemp-stack-ubuntu-nginx/).

We currently have an outdated version deployed [here](http://alicenbecca.com/)

# Setting up project on local machine

Instructional video
* https://www.youtube.com/watch?v=H3uRXvwXz1o&index=2&list=PLillGF-RfqbYhQsN5WMXy6VsDMKGadrJ-

To install: 
* [xamp](https://www.apachefriends.org/index.html) or [MAMP](https://www.mamp.info/en/)
* [composer](https://getcomposer.org/)
* [git if you have windows](https://gitforwindows.org/)
* [A code editor like visual studio](https://code.visualstudio.com/) or [php storm](https://www.jetbrains.com/phpstorm/)
* [Node js](https://nodejs.org/en/)


* git clone https://gitlab.cecs.anu.edu.au/u6060469/BballWebApp.git
* create MySQL database
cp .env.example .env

* edit .env 
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret
```
* composer update
* npm install
* php artisan key:generate


# Versions
Laravel Framework 5.6.38
Bootstrap 4
npm: '6.4.1',
http_parser: '2.8.0',
node: '6.14.3',


# Key stakeholders
We have identified six key stakeholders within the social competitions run on campus by the ANU Basketball Club.
1. [Competition manager](https://drive.google.com/open?id=1hefN0qh0B4MPzDCG2JAh4S65Vs60DTBnSxySg5H3ADY)
2. [Competition assistant manager](https://drive.google.com/open?id=1j-CkBP2mjRxXFdF0PUdUqq9TiHm8jUKg-V8qd01G6Lo)
3. [Referee manager](https://drive.google.com/open?id=1IscPWunk9VLrAJIAY-zeQ9vSxmujncKz0VYY26VwWnk)
4. [Referees](http://www.anubasketballclub.com/get_involved/referee/referee.html) 
5. Team managers
6. [Players](http://www.anubasketballclub.com/internal_competition/how_to_join/how_to_join.html)


# Document from semester 1.

|Type|Documents|
|:---:|:---:|
|Business overview|[poster](.documents-sem1-2018/poster.pdf)|
||[elevator pitch](.documents-sem1-2018/elevator-pitch.pdf)|
||[business model canvas](.documents-sem1-2018/business-model-canvas.pdf)|
||[business model pitch](.documents-sem1-2018/business-model-pitch.pdf)|
||[pain validation](.documents-sem1-2018/pain-validation.pdf)|
||[pain  validation playback and harvest](.documents-sem1-2018/pain-validation-playback-and-harvest-sem1-2018.pdf)|
|Mentor advice|[mentor advice](.documents-sem1-2018/mentor.pdf)|
|Research|[technology research](.documents-sem1-2018/technology-research.pdf)|
|Other documents produced|[er diagram](.documents-sem1-2018/er-model.pdf)|
||[sql database](.documents-sem1-2018/database-sql.pdf)|
||[flow map](.documents-sem1-2018/flow-map.pdf)|
||[user journey map](.documents-sem1-2018/user-journey-map.pdf)|
||[user story board](.documents-sem1-2018/user-story-board.pdf)|
||[Last semester's Google Drive](https://drive.google.com/open?id=1IYIhrGfbP5w2grQaXwNptR1yfGbtUQgZ)|
|Business overview|[poster](.documents-sem1-2018/poster.pdf)|


# Reflection for semester 1.
During semester 1, we focused on user research (finding out what our stakeholders really wants), requirements collection, user stories, prototype Design, stakeholder investigation, further analysis, system design, database design, and research on relevant technology.
So far, a series of detailed and specific user research have been finished thanks to our team and the help from our tutor Todd and mentor David and Zac. During these weeks, we have been familiar how the system should operate and we have output a series of diagram to not only guide the following technical development but also the documents to track potential change of requirements of our clients.



