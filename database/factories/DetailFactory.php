<?php

use Faker\Generator as Faker;
use App\Detail;

$factory->define(Detail::class, function (Faker $faker) {
    return [
        'title' => $faker->word,
        'content' => $faker->paragraph(),
        'competition_id' => $faker->randomDigit
    ];
});
