<?php

use Faker\Generator as Faker;
use App\Contact;

$factory->define(Contact::class, function (Faker $faker) {
    return [
        'name' => $faker->name(),
        'role' => $faker->word, 
        'mobile' => $faker->phoneNumber, 
        'email' => $faker->email, 
        'competition_id' => $faker->randomDigit
    ];
});
