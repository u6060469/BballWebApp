<?php

use Faker\Generator as Faker;
use App\Player;

$factory->define(Player::class, function (Faker $faker) {
    return [
        'competition_id' => $faker->randomDigit,
        'team_id' => $faker->randomDigit,
        'paid' => $faker->boolean(),
        'name' => $faker->name,
        'email' => $faker->email,
        'teammate' => $faker->name
    ];
});
